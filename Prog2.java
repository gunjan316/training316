
import java.util.*;
public class Prog2{
	public static void main(String args[]){
		Integer a[] = {1,2,3,4,5};
		ArrayList<Integer> al = new ArrayList<Integer>(Arrays.asList(a));

		Integer sum = al.stream().filter(n->n%2 == 0).reduce(0, Integer::sum);
		System.out.println(sum);

	}
}