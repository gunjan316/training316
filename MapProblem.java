package com.sapient.week1;
import java.util.*;

public class MapProblem {
	public static void main(String[] args) {
		Map<Integer,Set<String>> lhs = new LinkedHashMap<>();
		lhs.put(101, new LinkedHashSet<String>());
		lhs.get(101).add("BE");
		lhs.get(101).add("ME");
		lhs.put(102, new LinkedHashSet<String>());
		lhs.get(102).add("BE");
		System.out.println(lhs);
		System.out.println(lhs.keySet());
		System.out.println(lhs.values());
	}

}
