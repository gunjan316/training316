package com.mycompany.app;

public class EmployeeBean {
	private int id;
	private String name;
	private int salary;
	
	public EmployeeBean(int id, String name, int salary) {
		super();
		this.id = id;
		this.name = name;
		this.salary = salary;
	}
	public boolean equals(Object obj) {
		EmployeeBean eb = (EmployeeBean)obj;
		if((eb.id==id) && eb.name.equals(name) && eb.salary==salary){
			return true;
		}
		else return false;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}

}
