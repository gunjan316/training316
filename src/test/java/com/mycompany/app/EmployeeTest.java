package com.mycompany.app;
import static org.junit.Assert.*;


import org.junit.BeforeClass;
import org.junit.Test;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;
 
public class EmployeeTest {

	  private static EmployeeDOA mockedEmp;
	  
     
	@BeforeClass
	public static void setUp()
	{	public static List<EmployeeBean> al=null;
		mockedEmp = mock(EmployeeDOA.class);
		al = new ArrayList<>();
		al.add(new EmployeeBean(1,"shikhar",10000));
		al.add(new EmployeeBean(2,"rohit",20000));
		al.add(new EmployeeBean(3,"dhoni",12000));
		al.add(new EmployeeBean(4,"kohli",30000));
		al.add(new EmployeeBean(5,"sachin",100000));
		al.add(new EmployeeBean(6,"laxman",10000));
		al.add(new EmployeeBean(7,"dravid",80000));
		al.add(new EmployeeBean(8,"sehwag",10000));
		al.add(new EmployeeBean(9,"bumrah",40000));
		al.add(new EmployeeBean(10,"shami",57000));
		al.add(new EmployeeBean(11,"chahal",12000));
		al.add(new EmployeeBean(12,"kuldeep",10000));
		when(mockedEmp.readData()).thenReturn(al);
	}
//    @Test
//    public void compareEmployees(){
//        Object[] testOutput = mockedEmp.readData().toArray();
//        assertArrayEquals(expectedEmps, testOutput);
//    }
    
    @Test
    public void getTotSalc(){
    	float expected = 391000; 
    	assertEquals(expected,EmployeeDOA.getTotSal(al),0);
    }
    @Test
    public void countSalary(){
    	int expected = 1;
    	assertEquals(expected,EmployeeDOA.getCount(mockedEmp.readData(),90000));
    }
    @Test
    public void compareEmployee(){
        Object testOutput = (Object)EmployeeDOA.getEmployee(mockedEmp.readData(),3);
        assertEquals(new EmployeeBean(3,"dhoni",12000), testOutput);
    }
    
    
}
