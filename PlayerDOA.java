package com.sapient.week1;
import java.sql.*;
import java.util.*;
public class PlayerDOA {
	ResultSet rs = null;
	public List<PlayerBean> getPlayer(){
		List<PlayerBean> al = null;
		try {
		Connection con = new DBConnection().getConnection();
		PreparedStatement ps = con.prepareStatement("select id,fname,lname,jerseyno from playerdet");
		rs = ps.executeQuery();
		al = new ArrayList<>();
		while(rs.next()) {
			al.add(new PlayerBean(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4)));
		}
		return al;
		}catch (Exception e) {
			e.printStackTrace();
		}
	
		return al;
	}
	
	public List<PlayerBean> getAnyPlayer(int i){
		List<PlayerBean> al = null;
		try {
		Connection con = new DBConnection().getConnection();
		PreparedStatement ps = con.prepareStatement("select id,fname,lname,jerseyno from playerdet where id=?");
		ps.setInt(1,i);
		rs = ps.executeQuery();
		al = new ArrayList<>();
		while(rs.next()) {
			al.add(new PlayerBean(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4)));
		}
		return al;
		}catch (Exception e) {
			e.printStackTrace();
		}
	
		return al;
	}
	public void insertPlayer(int id, String fname, String lname, int jerseyno){
		try {
		Connection con = new DBConnection().getConnection();
		PreparedStatement ps = con.prepareStatement("insert into playerdet(id,fname,lname,jerseyno) values(?,?,?,?)");
		ps.setInt(1,id);
		ps.setString(2,fname);
		ps.setString(3, lname);
		ps.setInt(4, jerseyno);
		ps.executeUpdate();
		System.out.println("records inserted");
		}catch (Exception e) {
			e.printStackTrace();
		}
	
	}
	public void updatePlayer(int id){
		try {
		Connection con = new DBConnection().getConnection();
		PreparedStatement ps = con.prepareStatement("update playerdet set fname=? where id=?");
		ps.setString(1,"Shikhar");
		ps.setInt(2, id);
		ps.executeUpdate();
		System.out.println("records updated");
		}catch (Exception e) {
			e.printStackTrace();
		}
	
	}
	public void deletePlayer(String fname){
		try {
		Connection con = new DBConnection().getConnection();
		PreparedStatement ps = con.prepareStatement("delete from playerdet where fname=?");
		ps.setString(1,fname);
		ps.executeUpdate();
		System.out.println("records updated");
		}catch (Exception e) {
			e.printStackTrace();
		}
	
	}
	
	public void outerJoin(){
		try {
		Connection con = new DBConnection().getConnection();
		PreparedStatement ps = con.prepareStatement("select p.id,p.fname,p.lname,p.jerseyno,m.id,m.runs,m.mid from playerdet p full outer join matchdet m on p.id=m.id");
		
		rs = ps.executeQuery();
		int i = 0;
		while(rs.next()) {
			int id = rs.getInt(1);
			String fname = rs.getString(2);
			String lname = rs.getString(3);
			int jerseyno = rs.getInt(4);
			int mid = rs.getInt(5);
			int runs = rs.getInt(6);
			int matchid = rs.getInt(7);
			System.out.println("Player id is:: "+id+" First name:: "+fname+" Last Name:: "+lname+" Jersey no:: "+jerseyno+" runs:: "+runs+" match id:: "+matchid);
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
	
	}

}
