package com.sapient.week1;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
 
public class JUnit {
     
    Object[] expectedEmps = new Object[12];

	Object expectedEmpss = new EmployeeBean(3,"dhoni",12000);
     
    @Before
    public void initInputs(){
        expectedEmps[0] = new EmployeeBean(1,"shikhar",10000);
        expectedEmps[1] = new EmployeeBean(2,"rohit",20000);
        expectedEmps[2] = new EmployeeBean(3,"dhoni",12000);
        expectedEmps[3] = new EmployeeBean(4,"kohli",30000);
        expectedEmps[4] = new EmployeeBean(5,"sachin",100000);
        expectedEmps[5] = new EmployeeBean(6,"laxman",10000);
        expectedEmps[6] = new EmployeeBean(7,"dravid",80000);
        expectedEmps[7] = new EmployeeBean(8,"sehwag",10000);
        expectedEmps[8] = new EmployeeBean(9,"bumrah",40000);
        expectedEmps[9] = new EmployeeBean(10,"shami",57000);
        expectedEmps[10] = new EmployeeBean(11,"chahal",12000);
        expectedEmps[11] = new EmployeeBean(12,"kuldeep",10000);
    }
    
     
    @Test
    public void compareEmployees(){
        Object[] testOutput = EmployeeDOA.readData().toArray();
        assertArrayEquals(expectedEmps, testOutput);
    }
    
    @Test
    public void getTotSalc(){
    	float expected = 391000;
    	assertEquals(expected,EmployeeDOA.getTotSal(EmployeeDOA.readData()),0);
    }
    @Test
    public void countSalary(){
    	int expected = 1;
    	assertEquals(expected,EmployeeDOA.getCount(EmployeeDOA.readData(),90000));
    }
    @Test
    public void compareEmployee(){
        Object testOutput = (Object)EmployeeDOA.getEmployee(3);
        assertEquals(expectedEmpss, testOutput);
    }
    
    
}
