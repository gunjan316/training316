package com.sapient.week1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class EmployeeDOA {
	
	public static List<EmployeeBean> readData(){
		String csvFile = "Test.csv";
        String line = "";
        String cvsSplitBy = ",";
      
        List<EmployeeBean> al=new ArrayList<>();
        EmployeeBean empb = null;
			 try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
				 while ((line = br.readLine()) != null) {
		                String[] num = line.split(cvsSplitBy);
		                empb = new EmployeeBean(Integer.parseInt(num[0]),num[1],Integer.parseInt(num[2]));
		                al.add(empb);
		            }
		        } catch (IOException e) {
		            e.printStackTrace();
		        }return al;
		}
	
	public static float getTotSal(List<EmployeeBean> empbean) {
        float total = empbean.stream().collect(Collectors.summingInt(EmployeeBean::getSalary));
        return total;
	}
	public static int getCount(List<EmployeeBean> lo, int salary) {
		int count= (int)lo.stream().filter(e->e.getSalary()>salary).count();
		return count;
		}
	
	public static EmployeeBean getEmployee(int id) {
		List<EmployeeBean> lo = new ArrayList<>();
		lo = readData();
		EmployeeBean employee = null;
		for(EmployeeBean e : lo) {
			if(e.getId()==id)
				{ employee = e;
					break;}
				}
			return employee;
		}
}

	

