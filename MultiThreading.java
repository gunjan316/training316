package com.sapient.week1;

class Game{
	public synchronized void g1() {
		try {
			 notify();
			 System.out.print("Tick -" );
			 wait();
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
	public synchronized void g2() {
		try {
			 notify();
			 System.out.println("Tock -" );
			 wait();
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
}

class Player1 extends Thread{
	Game ob;
	public Player1(Game ob) {
		this.ob = ob;
	}
	public void run() {
		for(int i = 0; i < 10; i++) {
			ob.g1();
		}try {
			sleep(500);
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
}

class Player2 extends Thread{
	Game ob;
	public Player2(Game ob) {
		this.ob = ob;
	}
	public void run() {
		for(int i = 0; i < 10; i++) {
			ob.g2();
			if(i==0) {
				try {
					Thread.sleep(1000);
				}catch (InterruptedException e) {
						e.printStackTrace();
				}
				
			}
		}
	}
}

public class MultiThreading {
	public static void main(String[] args) {
		Game game = new Game();
		Player1 player1 = new Player1(game);
		Player2 player2 = new Player2(game);
		player1.start();
		player2.start();
	}
}
