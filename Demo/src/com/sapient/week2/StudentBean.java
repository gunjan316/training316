package com.sapient.week2;

public class StudentBean {
	
	private int id;
	
	private String name;
	
	private int rollno;
	
	private float percent;

	public StudentBean(int id, String name, int rollno, float percent) {
		super();
		this.id = id;
		this.name = name;
		this.rollno = rollno;
		this.percent = percent;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRollno() {
		return rollno;
	}

	public void setRollno(int rollno) {
		this.rollno = rollno;
	}

	public float getPercent() {
		return percent;
	}

	public void setPercent(float percent) {
		this.percent = percent;
	}

	@Override
	public String toString() {
		return "StudentBean [id=" + id + ", name=" + name + ", rollno=" + rollno + ", percentage=" + percent + "]";
	}

}
