package com.sapient.week2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class StudentHome
 */
public class StudentHome extends HttpServlet {
	private static final long serialVersionUID = 1L;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentHome() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		
		try {
			out.print("<html><center><body><h1>STUDENT HOME PAGE</h1>");
			out.print("<br><p align='right'><a href='LoginServlet'>LOGOUT</a></p>");
			out.print("<form action = 'StudentHome' method = 'post'>");
			out.print("<br>Enter Roll Number<input type = 'text' name = 'rollno' value='0' />");
			out.print("<br><input type = 'submit' value='Submit' />");
			out.print("</form></body></center></html>");
		}
		catch(Exception e) {
			
		}	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
	}

}
