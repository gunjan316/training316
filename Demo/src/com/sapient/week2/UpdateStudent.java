package com.sapient.week2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class UpdateStudent
 */
public class UpdateStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateStudent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
//			int n = request.getParameter(name)
//					int n = request.
			
			PrintWriter out = response.getWriter();
			out.print("<!DOCTYPE html><html><center><body>");
			out.print("<form action = 'InsertStudent' method = 'post'><h1>INSERT STUDENT</h1>");
			out.print("<br>Enter id<input type = 'number' name = 'id' value='0' />");
			out.print("<br>Enter name<input type = 'text' name = 'name' value='0' />");
			out.print("<br>Enter rollno<input type = 'number' name = 'rollno' value='0' />");
			out.print("<br>Enter username<input type = 'text' name = 'username' value='0' />");
			out.print("<br>Enter password<input type = 'password' name = 'password' value='0' />");
			out.print("<br>Enter percentage<input type = 'number' name = 'percent' value='0' />");
			out.print("<br><input type = 'submit' value='Add Student'/>");
			out.print("</form></body></center></html>");
		}
		catch(Exception e) {
			
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
