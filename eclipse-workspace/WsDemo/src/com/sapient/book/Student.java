package com.sapient.book;



import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.io.*;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;



@Path("/hello")
public class Student {

    @GET  
    @Produces(MediaType.TEXT_PLAIN)       
    public String getString1(){
        return "Welcome to CTS ACADEMY BY VIJAYA. INTRODUCTION TO REST";
    }
    @GET  
      @Path("/{param}")  
      public Response getMsg(@PathParam("param") String msg) {  
          String output = "Jersey say : " + msg;  
          return Response.status(200).entity(output).build();  
      }
    @GET
      
      @Path("sud/{param}")  
      public String getDetail(@PathParam("param") int msg) {  
        String output="";
        try {
            Connection co=DBConnection.getConnection();
            PreparedStatement ps=co.prepareStatement("select * from students where id=?");
            ps.setInt(1, msg);
            ResultSet rs=ps.executeQuery();
            if(rs.next()){
            	System.out.println(rs.getString(2)+"hi"+rs.getString(2));
                output=rs.getString(2)+" "+rs.getString(3)+" "+rs.getString(4); 
            }else{
                output="Invalid id";
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
         return output;  
      }
    @GET  
      @Path("{year}/{month}/{day}")  
      public Response getDate(  
              @PathParam("year") int year,  
              @PathParam("month") int month,   
              @PathParam("day") int day) {  
     
         String date = year + "/" + month + "/" + day;  
     
         return Response.status(200)  
          .entity("getDate is called, year/month/day : " + date)  
          .build();  
      } 
    
   
    
    @GET  
    @Path("user/{id}/{username}/{password}/{type}")  
    public Response addUser1(  
            @PathParam("id") String id,  
            @PathParam("username") String username,  
            @PathParam("password") String password,
            @PathParam("type") String type
           
          
            ) {  
   
       String msg = insertUser1(id,username,password,type);  
       System.out.println(msg);
   
       return Response.status(200)  
        .entity(msg)  
        .build();  
    } 
    @POST
    @Path("/adduser")  
    public Response addUserForm(  
        @FormParam("id") String id,  
        @FormParam("username") String username,  
        @FormParam("password") String password,
        @FormParam("type") String type
                  ) {  
   String msg=insert(id,username,password,type);
           
           return Response.status(200)  
            .entity(msg)  
            .build();  
    } 
  
  public String insertUser1(String ...a2)
  {
      try{
          Connection co=DBConnection.getConnection();
          PreparedStatement ps=co.prepareStatement("Insert into users(id,username,password,type) values(?,?,?,?)");
          for(int i=1;i<=a2.length;i++)
          {
              ps.setString(i, a2[i-1]);
          }
          ps.executeQuery();
          return "Inserted user";
      }
      catch (Exception e) {
          // TODO: handle exception
          return e.getMessage();
      }
  }
  
    
    
    @GET  
      @Path("student/{id}/{name}/{rollno}/{percent}")  
      public Response addStudent1(  
              @PathParam("id") String id,  
              @PathParam("name") String name,  
              @PathParam("rollno") String rollno,
              @PathParam("percent") String percent
             
            
              ) {  
     
         String msg = insert(id,name,rollno,percent);  
     
         return Response.status(200)  
          .entity(msg)  
          .build();  
      } 
    @POST
    @Path("/add1")  
    public Response addStudent(  
        @FormParam("id") String id,  
        @FormParam("name") String name,  
        @FormParam("rollno") String rollno,
        @FormParam("percent") String percent
                  ) {  
   String msg=insert(id,name,rollno,percent);
           
           return Response.status(200)  
            .entity(msg)  
            .build();  
    } 
    
    public String insert(String ...a1)
    {
        try{
            Connection co=DBConnection.getConnection();
            PreparedStatement ps=co.prepareStatement("Insert into students(id,name,rollno,percent) values(?,?,?,?)");
            for(int i=1;i<=a1.length;i++)
            {
                ps.setString(i, a1[i-1]);
            }
            ps.executeQuery();
            return "Inserted";
        }
        catch (Exception e) {
            // TODO: handle exception
            return e.getMessage();
        }
    }
    
    
    
    private static final String FILE_PATH1 = "C:\\Users\\gunmunde\\Desktop\\skull.jpg";  
      @GET  
      @Path("/image")  
      @Produces("image/jpg")  
      public Response getFile1() {  
          File file = new File(FILE_PATH1);  
          ResponseBuilder response = Response.ok((Object) file);  
          response.header("Content-Disposition","attachment; filename=\"skull.jpg");  
          return response.build();  
     
      }  
    
      private static final String FILE_PATH = "C:\\Users\\gunmunde\\Desktop\\Test.txt";  
      @GET  
      @Path("/txt")  
      @Produces("text/plain")  
      public Response getFile() {  
          File file = new File(FILE_PATH);  
     
          ResponseBuilder response = Response.ok((Object) file);  
          response.header("Content-Disposition","attachment; filename=\"Test.txt");  
          return response.build();  
     
      }
      
      @GET  
      @Path("/json/{id}")
      @Produces(MediaType.APPLICATION_JSON)  
      public Response sayXMLHello( @PathParam("id") String id) {  
          
          String output="";
            try {
                Connection co=DBConnection.getConnection();
                PreparedStatement ps=co.prepareStatement("select id,name,rollno,percent from students where id=?");
                ps.setString(1, id);
                ResultSet rs=ps.executeQuery();
                if(rs.next()){
                    output="{ \"name\" : \""+rs.getString(2)+"\", \"rollno\" : \""+rs.getString(3)
                    +"\", \"percent\" : \""+rs.getString(4)+"\"}"; 
                }else{
                    output="Invalid id";
                }
            } catch (Exception e) {
                // TODO: handle exception
            }
             return Response.status(200).entity(output).build();    
      }  
}