package com.sapient.week2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private UserDAO userDAO;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		PrintWriter out = response.getWriter();
		
		try {
			
			out.print("<html><center><body><h1>Login Application</h1>");
			out.print("<form action = 'LoginServlet' method = 'post'>");
			out.print("<br>Enter Username<input type = 'text' name = 'username' />");
			out.print("<br>Enter Password<input type = 'password' name = 'password' />");
			out.print("<br><input type = 'submit' value='Login' />");
			out.print("</body></center></html>");
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		userDAO = new UserDAO();
		PrintWriter out = response.getWriter();
		String result = userDAO.login(request.getParameter("username"),request.getParameter("password"));
		if(result.equals("a")) {
			response.sendRedirect("AdminHome");
		}
		else if(result.equals("s")) {
			response.sendRedirect("StudentHome");
		}
		else {
			out.print("invalid username/password");
		}
	}

}



// insert into users(username,)