package com.sapient.week2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class StudentDAO {

	ResultSet rs = null;
	ArrayList<StudentBean> al=null;
	public ArrayList<StudentBean> getStudentDetails() {
		try {
			al = new ArrayList<>();
			Connection con = new DBConnection().getConnection();
			PreparedStatement ps = con.prepareStatement("select id,name,rollno,percent from students");
			rs = ps.executeQuery();
			al = new ArrayList<>();
			while(rs.next()) {
			al.add(new StudentBean(rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getFloat(4)));
			}
			return al;
		} catch (Exception e) {
			// TODO: handle exception
		}return al;
		
	}
	
	public void updateDetails(UserBean ub, StudentBean sb) {
		try {
			Connection con = new DBConnection().getConnection();
			PreparedStatement ps = con.prepareStatement("update students set name=? and rollno=? and percent=? where id=?");
			ps.setString(1, sb.getName());
			ps.setInt(2, sb.getRollno());
			ps.setFloat(3, sb.getPercent());
			ps.setInt(4, sb.getId());
			int i = ps.executeUpdate();
			if(i > 0) System.out.println("Updated Student database");
			else System.out.println("student updation failed");
			
			ps = con.prepareStatement("update users set and username=? and password=? type=? where id=?");
			ps.setString(1, ub.getUsername());
			ps.setString(2, ub.getPassword());
			ps.setString(3, ub.getType());
			ps.setInt(4, ub.getId());
			int o = ps.executeUpdate();
			if(o > 0) System.out.println("Updated Student database");
			else System.out.println("student updation failed");

			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
