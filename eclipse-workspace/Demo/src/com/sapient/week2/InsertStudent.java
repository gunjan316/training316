package com.sapient.week2;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet implementation class InsertStudent
 */
public class InsertStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private UserBean user;   
    private UserDAO udao;
    private StudentBean student;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertStudent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		
		try {
			out.print("<!DOCTYPE html><html><center><body>");
			out.print("<form action = 'InsertStudent' method = 'post'><h1>INSERT STUDENT</h1>");
			out.print("<br>Enter id<input type = 'number' name = 'id' value='0' />");
			out.print("<br>Enter name<input type = 'text' name = 'name' value='0' />");
			out.print("<br>Enter rollno<input type = 'number' name = 'rollno' value='0' />");
			out.print("<br>Enter username<input type = 'text' name = 'username' value='0' />");
			out.print("<br>Enter password<input type = 'password' name = 'password' value='0' />");
			out.print("<br>Enter percentage<input type = 'number' name = 'percent' value='0' />");
			out.print("<br><input type = 'submit' value='Add Student'/>");
			out.print("</form></body></center></html>");
		}
		catch(Exception e) {
			
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		user = new UserBean(Integer.parseInt(request.getParameter("id")),request.getParameter("username"),request.getParameter("password"),"s");
		student = new StudentBean(Integer.parseInt(request.getParameter("id")),request.getParameter("name"),Integer.parseInt(request.getParameter("rollno")),Float.parseFloat(request.getParameter("percent")));
		new UserDAO().insertStudent(user, student);
PrintWriter out = response.getWriter();
		
		try {
//			out.print("<!DOCTYPE html><html><center><body>");
//			out.print("<form action = 'InsertStudent' method = 'post'><h1>INSERT STUDENT</h1>");
//			out.print("<br>Enter id<input type = 'number' name = 'id' value='0' />");
//			out.print("<br>Enter name<input type = 'text' name = 'name' value='0' />");
//			out.print("<br>Enter rollno<input type = 'number' name = 'rollno' value='0' />");
//			out.print("<br>Enter username<input type = 'text' name = 'username' value='0' />");
//			out.print("<br>Enter password<input type = 'password' name = 'password' value='0' />");
//			out.print("<br>Enter percentage<input type = 'number' name = 'percent' value='0' />");
//			out.print("<br><input type = 'submit' value='Add Student'/>");
//			out.print("</form></body></center></html>");
		}
		catch(Exception e) {
			
		}		
	}

}
