
import java.io.IOException;
import java.net.URI;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

/**
 * Servlet implementation class RestaurantListServlet
 */
public class Restaurant extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Restaurant() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		float lat = Float.parseFloat(request.getParameter("lat"));
		float lon = Float.parseFloat(request.getParameter("lon"));
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(getBaseUri());
		
		String output = target.path("geocode")
				.queryParam("lat", 28.4595)
				.queryParam("lon", 77.0266)
				.request()
				.header("user-key", "73bf72e931b98b31160e87d3d91651e1")
				.accept(MediaType.APPLICATION_JSON)
				.get(String.class);
		
		request.setAttribute("output", output);
		
		System.out.println(target.path("geocode")
				.queryParam("lat", lat)
				.request()
				.header("user-key", "73bf72e931b98b31160e87d3d91651e1")
				.accept(MediaType.TEXT_PLAIN)
				.get(String.class));
		
		RequestDispatcher rd = request.getRequestDispatcher("restos.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	private static URI getBaseUri() {
		return UriBuilder.fromUri("https://developers.zomato.com/api/v2.1").build();
	}

}
