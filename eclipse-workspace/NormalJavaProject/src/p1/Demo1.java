package p1;

import java.net.URI;
//import org.json.simple.JSONArray;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

public class Demo1 {

	public static void main(String[] args) {
		ClientConfig config = new ClientConfig();
		//Client client = ClientBuilder.newClient(config);
		WebTarget target = ClientBuilder.newClient().target(getBaseUri());
		//Enter the location
		//System.out.println(target.path("categories").request().header("user-key", "875dc8b727c2352a7aa0ae17b35676e5").accept(MediaType.TEXT_PLAIN).get(String.class));
		//System.out.println(target.path("locations").queryParam("query", "Gurgaon").request().header("user-key", "875dc8b727c2352a7aa0ae17b35676e5").accept(MediaType.TEXT_PLAIN).get(String.class));
		String s = target.path("geocode").queryParam("lat", "28.4595").queryParam("lon", "77.0266").request().header("user-key", "875dc8b727c2352a7aa0ae17b35676e5").accept(MediaType.TEXT_PLAIN).get(String.class);
		System.out.println(s);
		
		
	}
	private static URI getBaseUri() {
		return UriBuilder.fromUri("https://developers.zomato.com/api/v2.1/").build();
	}
}


//875dc8b727c2352a7aa0ae17b35676e5