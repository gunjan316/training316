package com.sapient.week1;

import java.io.RandomAccessFile;

import java.io.*;
public class Task {
   public static void main(String[] args) {
   
      try {
         // create a new RandomAccessFile with filename test
         RandomAccessFile raf = new RandomAccessFile("C:\\Users\\gunmunde\\Documents\\Hi.txt", "rw");

         // write something in the file
         raf.writeUTF("Hello World");

         // set the file pointer at 0 position
         raf.seek(0);

         // print the string
         System.out.println("" + raf.readUTF());

         // print current length
         System.out.println("" + raf.length());

         // set the file length to 30
         raf.setLength(1024*1024*1024);

         // print the new length
         System.out.println("" + raf.length());
         
//         List<String> alist = Files.lines(Paths.get(pathname))
//        		    .filter(line -> line.contains("abc"))
//        		    .collect(Collectors.toList());
         
      } catch (IOException ex) {
         ex.printStackTrace();
      }
   }
}
