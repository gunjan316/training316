package com.sapient.week1;

import java.util.*;
public class Database {

	public static void main(String[] args) {
		PlayerDOA pd = new PlayerDOA();
		List<PlayerBean> al = pd.getPlayer();
		System.out.println(al);
		pd.insertPlayer(70, "Jasprit", "bumrah", 93);
		List<PlayerBean> al1 = pd.getPlayer();
		System.out.println(al1);
		pd.updatePlayer(2);
		List<PlayerBean> al2 = pd.getPlayer();
		System.out.println(al2);
		pd.deletePlayer("Shikhar");
		List<PlayerBean> al3 = pd.getPlayer();
		System.out.println(al3);
		pd.outerJoin();
		
	}
}

