package p1;

public class HolidayBean {
	private int date;
	private String day;
	
	
	public HolidayBean(int date, String day) {
		super();
		this.date = date;
		this.day = day;
	}
	public int getDate() {
		return date;
	}
	public void setDate(int date) {
		this.date = date;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	@Override
	public String toString() {
		return "HolidayBean [date=" + date + ", day=" + day + "]";
	}
	
	public void display() {
	System.out.println(date+" is the data and the day is "+day);
	}
	

}
