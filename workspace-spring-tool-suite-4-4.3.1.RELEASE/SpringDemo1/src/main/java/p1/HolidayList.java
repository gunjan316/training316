package p1;

import java.util.ArrayList;
import java.util.*;

public class HolidayList {
	private List<HolidayBean> al = new ArrayList<HolidayBean>();
	
	


	public HolidayList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public HolidayList(List<HolidayBean> al) {
		super();
		this.al = al;
	}

	public List<HolidayBean> getAl() {
		return al;
	}

	public void setAl(List<HolidayBean> al) {
		this.al = al;
	}

	@Override
	public String toString() {
		return "HolidayList [al=" + al + "]";
	}
	

	

}
