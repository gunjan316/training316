package p1;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class JavaContainer {
	@Bean
	@Scope("singleton")
	public Hello get1() {
		return new Hello();
	}
	@Bean
	public Employee getEmployee() {
		Employee ob;
		ob = new Employee(12,"Hitler","Germany");
		return ob;
	}
	
	@Bean
	public HolidayList get3() {
		HolidayList ob;
		ob = new HolidayList();
		ob.getAl().add(new HolidayBean(12,"birthday"));
		ob.getAl().add(new HolidayBean(13,"hiii"));
		return ob;
	}

}
