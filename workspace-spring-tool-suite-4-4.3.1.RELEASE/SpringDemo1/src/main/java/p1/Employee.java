package p1;

public class Employee {
	private int age;
	private String name;
	private String city;
	
	public Employee(int age, String name,String city) {
		super();
		this.age = age;
		this.name = name;
		this.city = city;
	}
	
	public Employee() {
		super();
	}

	@Override
	public String toString() {
		return "Employee [age=" + age + ", name=" + name + ", city=" + city + "]";
	}


	
	
}
