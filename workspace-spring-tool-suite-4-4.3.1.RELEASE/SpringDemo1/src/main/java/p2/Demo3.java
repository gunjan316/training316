package p2;

import org.aspectj.lang.annotation.Before;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Demo3 {
	public static void main(String[] args) {
		try {
			Arithmetic ob;
			ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
			ob = (Arithmetic) context.getBean("arithmetic");
			
			System.out.println(ob.add(100.0,20.3));
			System.out.println(ob.sub(45, 22.3));
			System.out.println(ob.mul(120., 2.3));;
			System.out.println(ob.div(45.0, 5));;
			
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
