package p2;

public interface Arithmetic {
	public double add(double num1,double num2);
	public double sub(double num1, double num2);
	public double mul(double num1, double num2);
	public double div(double num1, double num2);

}
//<context:annotation-config/>
//<aop:aspectj-autoproxy/>   
//
//<bean id="arithmetic" class="p2.Arithmetic"></bean>
//<bean id="aspect" class = "p2.ArithmeticAspect"></bean>
     