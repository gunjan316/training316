package com.example.demo;

import java.io.Serializable;

public class Add implements Serializable {
	private static final long serialVersionUID = 1L;
	transient int num1;
	transient int num2;
	transient int num3;
	
	public void setData(int num1,int num2) {
		this.num1=num1;
		this.num2=num2;
	}
	public void cal() {
		num3 = num1 + num2;
	}
	public void display() {
		System.out.println("Num1= "+num1+" num2 ="+num2+" num3"+num3);
	}
}
