package com.example.demo;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class DeSerializaionDemo {
	public static void main(String[] args) throws Exception {
		FileInputStream stream = new FileInputStream("file.dat");
		ObjectInputStream in = new ObjectInputStream(stream);
		Add ob = (Add) in.readObject();
		ob.display();
		in.close();
	}

}
