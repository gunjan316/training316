package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
	@Autowired
	StudentDetailsDAO sdetails;
	@Autowired
	StudentMarksDAO smarks;
	
	@PostMapping("/addS")
	public String storeStudentDetails(@RequestBody StudentDetails ob) {
		sdetails.save(ob);
		return "saved student details";
	}
	@PostMapping("/addM")
	public String storeStudentMarks(@RequestBody StudentMarks ob) {
		smarks.save(ob);
		return "saved marks";
	}
	@GetMapping("/displayS")
	public List<StudentDetails> displayS(){
		return (List<StudentDetails>) sdetails.findAll();
	}
	@GetMapping("/displayM")
	public List<StudentMarks> displayM(){
		return (List<StudentMarks>) smarks.findAll();
	}
	
}
