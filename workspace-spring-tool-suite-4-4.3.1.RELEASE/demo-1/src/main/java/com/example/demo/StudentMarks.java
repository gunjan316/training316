package com.example.demo;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Transient;


@Entity
public class StudentMarks {
	
	
	@Id
	private String id;
	@Column
	private String regno;
	@Column
	private int testno;
	@Column
	private int mark1;
	@Column
	private int mark2;
	@Column
	private int mark3;
	
	@Transient
	@OneToMany
	@JoinColumn(name="regno")
	List<StudentDetails> sref;
	
	public List<StudentDetails> getSref() {
		return sref;
	}
	public void setSref(List<StudentDetails> sref) {
		this.sref = sref;
	}
	public String getRegno() {
		return regno;
	}
	public void setRegno(String regno) {
		this.regno = regno;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getTestno() {
		return testno;
	}
	public void setTestno(int testno) {
		this.testno = testno;
	}
	public int getMark1() {
		return mark1;
	}
	public void setMark1(int mark1) {
		this.mark1 = mark1;
	}
	public int getMark2() {
		return mark2;
	}
	public void setMark2(int mark2) {
		this.mark2 = mark2;
	}
	public int getMark3() {
		return mark3;
	}
	public void setMark3(int mark3) {
		this.mark3 = mark3;
	}

}
