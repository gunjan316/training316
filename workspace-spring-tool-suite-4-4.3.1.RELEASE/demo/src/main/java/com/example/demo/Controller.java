package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
	@Autowired
	StudentDAO ob;
	
	@GetMapping("/hello")
	public String display() {
		return "Hi microservices";
	}
	
	@GetMapping("/hello1")
	public Student studDisplay() {
		return new Student("gunjan",22,"Mumbai");
	}
	@GetMapping("/student")
	public List<Student> getStudents(){
		return ob.getStudent();
	}
	@GetMapping("/student/{name}")
	public List<Student> getStudents2(@PathVariable String name){
		return ob.getDetails(name);
	}
	@PostMapping("/student")
	public String insert(@RequestBody Student bean) {
		return ob.insert(bean);
	}
	@DeleteMapping("/student/{name}")
	public String delete(@PathVariable String name) {
		boolean a = ob.delete(name);
		return a?"Deleted successfully":"cant delete";
	}
	
	@PutMapping("/student/{name}")
	public String update(@PathVariable String name, @RequestBody Student bean) {
		return ob.update(name, bean);
	}
}
