package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class StudentList {
	private List<Student> list;

	public StudentList() {
		super();
		list = new ArrayList<>();
		list.add(new Student("Rutu",12,"Vasai"));
		list.add(new Student("chaitu",11,"Virar"));
		list.add(new Student("Tanu",18,"nsp"));
		list.add(new Student("kitu",23,"parbhani"));
	}

	public List<Student> getList() {
		return list;
	}

	public void setList(List<Student> list) {
		this.list = list;
	}
	
	
}
