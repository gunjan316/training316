package com.sapient.week1;
import java.util.*;

class Employee implements Comparator<Employee> {
    private int id;
    private String name;
    private int age;
     List al = new ArrayList();

    Employee() {
    }

    public List getAl() {
        return al;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


    Employee(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public void addDetails(Employee e) {
        al.add(e);
    }

    public Employee getDetails(int id) {
//    	
        Iterator itr = al.iterator();
        while (itr.hasNext()) {
            Employee ep = (Employee) itr.next();
            if (ep.id == id) {
                return ep;
            }
        }
        return null;
    }



//    @Override
    
    public int compare(Employee o1, Employee o2) {
        return (o1.getName().compareTo(o2.getName()));
    }
    
    @Override
    
    public String toString() {
    	return "Id is "+this.id+" Name is "+this.name+" Age is "+this.age;
    }
    
}
public class Driver{
    public static void main(String[] args) {
        
        Employee e = new Employee();
        Employee emp1 = new Employee(3,"ron",21);
        Employee emp2 = new Employee(2,"jon",25);
        Employee emp3 = new Employee(5,"shawn",22);
        e.addDetails(emp1);
        e.addDetails(emp2);
        e.addDetails(emp3);
        System.out.println("enter id to get details");
        int id = Read.sc.nextInt();
        Employee det = e.getDetails(id);
        System.out.println(det.getAge());
        System.out.println(det.getName());
        System.out.println("printing name sorted list");
        Collections.sort(e.getAl(), e);
        System.out.println(e.getAl());
//        Iterator itr = e.getAl().iterator();
//        while(itr.hasNext()){
//            Employee emp = (Employee)itr.next();
//            System.out.println("Emp name "+emp.getName()+" Emp age "+emp.getAge()+" emp id "+emp.getId());
//        }
        
    }
}
